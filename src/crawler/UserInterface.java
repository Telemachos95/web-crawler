package crawler;

import java.io.IOException;
import java.util.Scanner;

public class UserInterface {

    public void UI() throws IOException {

        Wrapper FindProduct = new Wrapper();
        System.out.println("Please provide a keyword:");
        Scanner input = new Scanner(System.in);
        String keyword = input.nextLine();
        System.out.println("\n");
        System.out.println("Search category:");
        System.out.println("1.Cpu's");
        System.out.println("2.GraphicsCard");
        System.out.println("3.HardDiskDrives");
        System.out.println("4.Motherboards");
        System.out.println("5.Ram");
        System.out.println("6.Soundcards");
        System.out.println("7.Cases");
        System.out.println("8.PSU");
        System.out.println("9.Read and Write");
        System.out.println("10.Network cards");
        System.out.println("11.All Categories");

        int choice = input.nextInt();

        if (choice == 1) {
            String url = "http://www.plaisio.gr/search.aspx?keyword=findproduct&CategoryDisplayName=%CE%95%CF%80%CE%B5%CE%BE%CE%B5%CF%81%CE%B3%CE%B1%CF%83%CF%84%CE%AD%CF%82%20(CPU)&sort=priceasc";
            String search = url.replaceAll("findproduct", keyword);
            FindProduct.ProductsWrapper(search);
            FindProduct.PrintProducts();
        }

        if (choice == 2) {
            String url = "http://www.plaisio.gr/search.aspx?keyword=findproduct&CategoryDisplayName=%CE%9A%CE%AC%CF%81%CF%84%CE%B5%CF%82%20%CE%B3%CF%81%CE%B1%CF%86%CE%B9%CE%BA%CF%8E%CE%BD%20(VGA)&sort=priceasc";
            String search = url.replaceAll("findproduct", keyword);
            FindProduct.ProductsWrapper(search);
            FindProduct.PrintProducts();
        }

        if (choice == 3) {
            String url = "http://www.plaisio.gr/search.aspx?keyword=findproduct&CategoryDisplayName=%CE%A3%CE%BA%CE%BB%CE%B7%CF%81%CE%BF%CE%AF%20%CE%B4%CE%AF%CF%83%CE%BA%CE%BF%CE%B9%20(HDD-SSD)&sort=priceasc";
            String search = url.replaceAll("findproduct", keyword);
            FindProduct.ProductsWrapper(search);
            FindProduct.PrintProducts();
        }

        if (choice == 4) {
            String url = "http://www.plaisio.gr/search.aspx?keyword=findproduct&CategoryDisplayName=M%CE%B7%CF%84%CF%81%CE%B9%CE%BA%CE%AD%CF%82&sort=priceasc";
            String search = url.replaceAll("findproduct", keyword);
            FindProduct.ProductsWrapper(search);
            FindProduct.PrintProducts();
        }

        if (choice == 5) {
            String url = "http://www.plaisio.gr/search.aspx?keyword=findproduct&CategoryDisplayName=%CE%9C%CE%BD%CE%AE%CE%BC%CE%B5%CF%82%20RAM&sort=priceasc";
            String search = url.replaceAll("findproduct", keyword);
            FindProduct.ProductsWrapper(search);
            FindProduct.PrintProducts();
        }

        if (choice == 6) {
            String url = "http://www.plaisio.gr/search.aspx?keyword=findproduct&CategoryDisplayName=%CE%9A%CE%AC%CF%81%CF%84%CE%B5%CF%82%20%CE%AE%CF%87%CE%BF%CF%85&sort=priceasc";
            String search = url.replaceAll("findproduct", keyword);
            FindProduct.ProductsWrapper(search);
            FindProduct.PrintProducts();
        }

        if (choice == 7) {
            String url = "http://www.plaisio.gr/search.aspx?keyword=findproduct&CategoryDisplayName=%CE%9A%CE%BF%CF%85%CF%84%CE%B9%CE%AC%20(Cases)&sort=priceasc";
            String search = url.replaceAll("findproduct", keyword);
            FindProduct.ProductsWrapper(search);
            FindProduct.PrintProducts();
        }

        if (choice == 8) {
            String url = "http://www.plaisio.gr/search.aspx?keyword=findproduct&CategoryDisplayName=%CE%A4%CF%81%CE%BF%CF%86%CE%BF%CE%B4%CE%BF%CF%84%CE%B9%CE%BA%CE%AC%20(PSU)&sort=priceasc";
            String search = url.replaceAll("findproduct", keyword);
            FindProduct.ProductsWrapper(search);
            FindProduct.PrintProducts();
        }

        if (choice == 9) {
            String url = "http://www.plaisio.gr/search.aspx?keyword=findproduct&CategoryDisplayName=%CE%9F%CF%80%CF%84%CE%B9%CE%BA%CE%AC%20(CD-DVD)&sort=priceasc";
            String search = url.replaceAll("findproduct", keyword);
            FindProduct.ProductsWrapper(search);
            FindProduct.PrintProducts();
        }
        
        if (choice == 10) {
            String url = "http://www.plaisio.gr/search.aspx?keyword=findproduct&CategoryDisplayName=WiFi%20Sticks%20-%20%CE%9A%CE%AC%CF%81%CF%84%CE%B5%CF%82%20%CE%94%CE%B9%CE%BA%CF%84%CF%8D%CE%BF%CF%85&sort=priceasc";
            String search = url.replaceAll("findproduct", keyword);
            FindProduct.ProductsWrapper(search);
            FindProduct.PrintProducts();
        }
        
        if (choice == 11) {
            String url = "http://www.plaisio.gr/search.aspx?keyword=findproduct";
            String search = url.replaceAll("findproduct", keyword);
            FindProduct.ProductsWrapper(search);
            FindProduct.PrintProducts();
        }
    }
}
