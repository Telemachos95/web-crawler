package crawler;

import java.io.IOException;
import java.util.ArrayList;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import productcategories.Product;

public class Wrapper {

    ArrayList<Product> ProductList = new ArrayList();

    public void GetAllProducts(String url, String keyword) throws IOException {
        Document doc = Jsoup.connect(url).userAgent("Chrome").get();
        String nextUrl = doc.select("[class=rightButtonPagination]").select("*").attr("abs:href");
        if (nextUrl.isEmpty()) {
            return;
        }
        if (keyword.matches("graphics")) {
            ProductsWrapper(nextUrl);
        }
    }

    public void PrintProducts() {
        System.out.println("Search Results(" + ProductList.size() / 2 + "):\n");
        for (Product showproduct : ProductList) {
            System.out.println(showproduct.getName() + "\t" + showproduct.getPrice() + showproduct.getCode());
        }
    }

    public void ProductsWrapper(String url) throws IOException {

        Document doc = Jsoup.connect(url).userAgent("Chrome").get();
        Elements Products = doc.select("div.product-info,div.product-price");
        for (Element info : Products) {
            String name = info.select("h2").text().trim();
            String price = info.select("div.product-price").text().replaceAll("στα καταστήματα", "").replaceAll("Διαθέσιμο μόνο", "").trim();
            String code = info.select("div.text").select("div.pdgt").text().trim();
            String availability = info.select("ul.additional-info").text().trim();
            System.out.println(availability);
            Elements fixphoto = info.select("[class=photo]");
            String photo = fixphoto.attr("abs:src");
            Product newProduct = new Product(price, name, code, 0, availability, "Πλαίσιο", "", photo);
            ProductList.add(newProduct);
        }
        GetAllProducts(url, "graphics");

    }
}
