package productcategories;

public class GraphicsCard extends Product {
 
    private String ProcessorModel;
    private String Interface;
    private String MemorySize;
    private String MemoryType;
    private String ProcessorSpeed;
    private String MemorySpeed;
    private String Consumption;
    private boolean HdmiConnection;
    private boolean VgaConnection;

    public GraphicsCard(String ProcessorModel, String Interface, String MemorySize, String MemoryType, String ProcessorSpeed, String MemorySpeed, String Consumption, boolean HdmiConnection, boolean VgaConnection, String price, String name, String code, int guarantee, String availability, String store, String manufacturer, String photo) {
        super(price, name, code, guarantee, availability, store, manufacturer, photo);
        this.ProcessorModel = ProcessorModel;
        this.Interface = Interface;
        this.MemorySize = MemorySize;
        this.MemoryType = MemoryType;
        this.ProcessorSpeed = ProcessorSpeed;
        this.MemorySpeed = MemorySpeed;
        this.Consumption = Consumption;
        this.HdmiConnection = HdmiConnection;
        this.VgaConnection = VgaConnection;
    }

    

    public GraphicsCard(String price, String name, String code, int guarantee, String availability, String store, String manufacturer, String photo) {
        super(price, name, code, guarantee, availability, store, manufacturer, photo);
    }

    
    
    

    public String getProcessorModel() {
        return ProcessorModel;
    }

    public void setProcessorModel(String ProcessorModel) {
        this.ProcessorModel = ProcessorModel;
    }

    public String getInterface() {
        return Interface;
    }

    public void setInterface(String Interface) {
        this.Interface = Interface;
    }

    public String getMemorySize() {
        return MemorySize;
    }

    public void setMemorySize(String MemorySize) {
        this.MemorySize = MemorySize;
    }

    public String getMemoryType() {
        return MemoryType;
    }

    public void setMemoryType(String MemoryType) {
        this.MemoryType = MemoryType;
    }

    public String getProcessorSpeed() {
        return ProcessorSpeed;
    }

    public void setProcessorSpeed(String ProcessorSpeed) {
        this.ProcessorSpeed = ProcessorSpeed;
    }

    public String getMemorySpeed() {
        return MemorySpeed;
    }

    public void setMemorySpeed(String MemorySpeed) {
        this.MemorySpeed = MemorySpeed;
    }

    public String getConsumption() {
        return Consumption;
    }

    public void setConsumption(String Consumption) {
        this.Consumption = Consumption;
    }

    public boolean isHdmiConnection() {
        return HdmiConnection;
    }

    public void setHdmiConnection(boolean HdmiConnection) {
        this.HdmiConnection = HdmiConnection;
    }

    public boolean isVgaConnection() {
        return VgaConnection;
    }

    public void setVgaConnection(boolean VgaConnection) {
        this.VgaConnection = VgaConnection;
    }
    
    
}