
package productcategories;

public class readNwrite extends Product {
    private String readType;
    private String writeType;
    private boolean isExternal;
    private String usage;

    public readNwrite(String readType, String writeType, boolean isExternal, String usage, String price, String name, String code, int guarantee, String availability, String store, String manufacturer, String photo) {
        super(price, name, code, guarantee, availability, store, manufacturer, photo);
        this.readType = readType;
        this.writeType = writeType;
        this.isExternal = isExternal;
        this.usage = usage;
    }
    
    

    public String getReadType() {
        return readType;
    }

    public void setReadType(String readType) {
        this.readType = readType;
    }

    public String getWriteType() {
        return writeType;
    }

    public void setWriteType(String writeType) {
        this.writeType = writeType;
    }

    public boolean isIsExternal() {
        return isExternal;
    }

    public void setIsExternal(boolean isExternal) {
        this.isExternal = isExternal;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }
    
    
}

