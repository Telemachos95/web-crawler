package productcategories;

public class SoundCard extends Product {
    
    private boolean internal;
    private String intrfc; //stands for interface
    private float channel;
    private String soundquality;

    public SoundCard(boolean internal, String intrfc, float channel, String soundquality, String price, String name, String code, int guarantee, String availability, String store, String manufacturer, String photo) {
        super(price, name, code, guarantee, availability, store, manufacturer, photo);
        this.internal = internal;
        this.intrfc = intrfc;
        this.channel = channel;
        this.soundquality = soundquality;
    }

    
    

    public boolean isInternal() {
        return internal;
    }

    public void setInternal(boolean internal) {
        this.internal = internal;
    }

    public String getIntrfc() {
        return intrfc;
    }

    public void setIntrfc(String intrfc) {
        this.intrfc = intrfc;
    }

    public float getChannel() {
        return channel;
    }

    public void setChannel(float channel) {
        this.channel = channel;
    }

    public String getSoundquality() {
        return soundquality;
    }

    public void setSoundquality(String soundquality) {
        this.soundquality = soundquality;
    }
    
    
    
}