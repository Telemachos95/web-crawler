package productcategories;

public class Case extends Product {
    private String size;
    private String type;
    private int height; //monada se mm
    private int width;
    private int depth;
    private String colour;
    private String driveSlots;
    private int usbSlots;
    private int weight; //monada se kg

    public Case(String size, String type, int height, int width, int depth, String colour, String driveSlots, int usbSlots, int weight, String price, String name, String code, int guarantee, String availability, String store, String manufacturer, String photo) {
        super(price, name, code, guarantee, availability, store, manufacturer, photo);
        this.size = size;
        this.type = type;
        this.height = height;
        this.width = width;
        this.depth = depth;
        this.colour = colour;
        this.driveSlots = driveSlots;
        this.usbSlots = usbSlots;
        this.weight = weight;
    }

    

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getDriveSlots() {
        return driveSlots;
    }

    public void setDriveSlots(String driveSlots) {
        this.driveSlots = driveSlots;
    }

    public int getUsbSlots() {
        return usbSlots;
    }

    public void setUsbSlots(int usbSlots) {
        this.usbSlots = usbSlots;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
    
    
}
