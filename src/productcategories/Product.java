package productcategories;

public class Product {
    
    private String price;
    private String name;
    private String code;
    private int guarantee;
    private String availability;
    private String store;
    private String manufacturer;
    private String photo;

    public Product(String price, String name, String code, int guarantee, String availability, String store, String manufacturer, String photo) {
        this.price = price;
        this.name = name;
        this.code = code;
        this.guarantee = guarantee;
        this.availability = availability;
        this.store = store;
        this.manufacturer = manufacturer;
        this.photo = photo;
    }

    
    public String getPrice() {
        return price;
    }

   
    public void setPrice(String price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getGuarantee() {
        return guarantee;
    }

    public void setGuarantee(int guarantee) {
        this.guarantee = guarantee;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
    
    
    
}