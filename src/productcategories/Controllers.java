package productcategories;

public class Controllers extends Product {
    private String internalPorts;
    private String externalPorts;
    private String hostintrfc; //stands for host interface

    public Controllers(String internalPorts, String externalPorts, String hostintrfc, String price, String name, String code, int guarantee, String availability, String store, String manufacturer, String photo) {
        super(price, name, code, guarantee, availability, store, manufacturer, photo);
        this.internalPorts = internalPorts;
        this.externalPorts = externalPorts;
        this.hostintrfc = hostintrfc;
    }

    
    

    public String getInternalPorts() {
        return internalPorts;
    }

    public void setInternalPorts(String internalPorts) {
        this.internalPorts = internalPorts;
    }

    public String getExternalPorts() {
        return externalPorts;
    }

    public void setExternalPorts(String externalPorts) {
        this.externalPorts = externalPorts;
    }

    public String getHostintrfc() {
        return hostintrfc;
    }

    public void setHostintrfc(String hostintrfc) {
        this.hostintrfc = hostintrfc;
    }
    
    
}