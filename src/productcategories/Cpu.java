package productcategories;

public class Cpu extends Product{
    private int frequency;//monada se Hz
    private int socket;
    private boolean bit64; //dhl. an einai h den einai
    private int numberOfCores;
    private String packing; //to package einai desmeymenh leksh
    private float cacheMemory;
    private int thermalPower; //monada se Watt
    private int maxRam; // maximum ram supported
    private String dimensions; // megethos toy paketoy px 40,5x30,5mm

    public Cpu(int frequency, int socket, boolean bit64, int numberOfCores, String packing, float cacheMemory, int thermalPower, int maxRam, String dimensions, String price, String name, String code, int guarantee, String availability, String store, String manufacturer, String photo) {
        super(price, name, code, guarantee, availability, store, manufacturer, photo);
        this.frequency = frequency;
        this.socket = socket;
        this.bit64 = bit64;
        this.numberOfCores = numberOfCores;
        this.packing = packing;
        this.cacheMemory = cacheMemory;
        this.thermalPower = thermalPower;
        this.maxRam = maxRam;
        this.dimensions = dimensions;
    }

    

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public int getSocket() {
        return socket;
    }

    public void setSocket(int socket) {
        this.socket = socket;
    }

    public boolean isBit64() {
        return bit64;
    }

    public void setBit64(boolean bit64) {
        this.bit64 = bit64;
    }

    public int getNumberOfCores() {
        return numberOfCores;
    }

    public void setNumberOfCores(int numberOfCores) {
        this.numberOfCores = numberOfCores;
    }

    public String getPacking() {
        return packing;
    }

    public void setPacking(String packing) {
        this.packing = packing;
    }

    public float getCacheMemory() {
        return cacheMemory;
    }

    public void setCacheMemory(float cacheMemory) {
        this.cacheMemory = cacheMemory;
    }

    public int getThermalPower() {
        return thermalPower;
    }

    public void setThermalPower(int thermalPower) {
        this.thermalPower = thermalPower;
    }

    public int getMaxRam() {
        return maxRam;
    }

    public void setMaxRam(int maxRam) {
        this.maxRam = maxRam;
    }

    public String getDimensions() {
        return dimensions;
    }

    public void setDimensions(String dimensions) {
        this.dimensions = dimensions;
    }
       
}
