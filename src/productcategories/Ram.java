package productcategories;

public class Ram extends Product {
    
    private String Capacity;
    private String MemoryType;
    private String MemorySpeed;
    private String ComputerType;
    private String Size;

    public Ram(String Capacity, String MemoryType, String MemorySpeed, String ComputerType, String Size, String price, String name, String code, int guarantee, String availability, String store, String manufacturer, String photo) {
        super(price, name, code, guarantee, availability, store, manufacturer, photo);
        this.Capacity = Capacity;
        this.MemoryType = MemoryType;
        this.MemorySpeed = MemorySpeed;
        this.ComputerType = ComputerType;
        this.Size = Size;
    }

    

    

    public String getCapacity() {
        return Capacity;
    }

    public void setCapacity(String Capacity) {
        this.Capacity = Capacity;
    }

    public String getMemoryType() {
        return MemoryType;
    }

    public void setMemoryType(String MemoryType) {
        this.MemoryType = MemoryType;
    }

    public String getMemorySpeed() {
        return MemorySpeed;
    }

    public void setMemorySpeed(String MemorySpeed) {
        this.MemorySpeed = MemorySpeed;
    }

    public String getComputerType() {
        return ComputerType;
    }

    public void setComputerType(String ComputerType) {
        this.ComputerType = ComputerType;
    }

    public String getSize() {
        return Size;
    }

    public void setSize(String Size) {
        this.Size = Size;
    }
    
    
    
}