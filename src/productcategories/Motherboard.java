package productcategories;

public class Motherboard extends Product {
    
    private String Mobosize;
    private String SupportedProcessors;
    private String ChipsetManufacturer;
    private String Socket;
    private String SupportedMemory;
    private int DimmSlots;
    private String SupportedMemorySpeed;
    private String ComputerType;
    private int Usb2Ports;
    private int Usb3Ports;
    private boolean Hdmi;
    private boolean Ethernet;
    private boolean Wifi;
    private boolean SoundCard;

    public Motherboard(String Mobosize, String SupportedProcessors, String ChipsetManufacturer, String Socket, String SupportedMemory, int DimmSlots, String SupportedMemorySpeed, String ComputerType, int Usb2Ports, int Usb3Ports, boolean Hdmi, boolean Ethernet, boolean Wifi, boolean SoundCard, String price, String name, String code, int guarantee, String availability, String store, String manufacturer, String photo) {
        super(price, name, code, guarantee, availability, store, manufacturer, photo);
        this.Mobosize = Mobosize;
        this.SupportedProcessors = SupportedProcessors;
        this.ChipsetManufacturer = ChipsetManufacturer;
        this.Socket = Socket;
        this.SupportedMemory = SupportedMemory;
        this.DimmSlots = DimmSlots;
        this.SupportedMemorySpeed = SupportedMemorySpeed;
        this.ComputerType = ComputerType;
        this.Usb2Ports = Usb2Ports;
        this.Usb3Ports = Usb3Ports;
        this.Hdmi = Hdmi;
        this.Ethernet = Ethernet;
        this.Wifi = Wifi;
        this.SoundCard = SoundCard;
    }

    

    

    public String getMobosize() {
        return Mobosize;
    }

    public void setMobosize(String Mobosize) {
        this.Mobosize = Mobosize;
    }

    public String getSupportedProcessors() {
        return SupportedProcessors;
    }

    public void setSupportedProcessors(String SupportedProcessors) {
        this.SupportedProcessors = SupportedProcessors;
    }

    public String getChipsetManufacturer() {
        return ChipsetManufacturer;
    }

    public void setChipsetManufacturer(String ChipsetManufacturer) {
        this.ChipsetManufacturer = ChipsetManufacturer;
    }

    public String getSocket() {
        return Socket;
    }

    public void setSocket(String Socket) {
        this.Socket = Socket;
    }

    public String getSupportedMemory() {
        return SupportedMemory;
    }

    public void setSupportedMemory(String SupportedMemory) {
        this.SupportedMemory = SupportedMemory;
    }

    public int getDimmSlots() {
        return DimmSlots;
    }

    public void setDimmSlots(int DimmSlots) {
        this.DimmSlots = DimmSlots;
    }

    public String getSupportedMemorySpeed() {
        return SupportedMemorySpeed;
    }

    public void setSupportedMemorySpeed(String SupportedMemorySpeed) {
        this.SupportedMemorySpeed = SupportedMemorySpeed;
    }

    public String getComputerType() {
        return ComputerType;
    }

    public void setComputerType(String ComputerType) {
        this.ComputerType = ComputerType;
    }

    public int getUsb2Ports() {
        return Usb2Ports;
    }

    public void setUsb2Ports(int Usb2Ports) {
        this.Usb2Ports = Usb2Ports;
    }

    public int getUsb3Ports() {
        return Usb3Ports;
    }

    public void setUsb3Ports(int Usb3Ports) {
        this.Usb3Ports = Usb3Ports;
    }

    public boolean isHdmi() {
        return Hdmi;
    }

    public void setHdmi(boolean Hdmi) {
        this.Hdmi = Hdmi;
    }

    public boolean isEthernet() {
        return Ethernet;
    }

    public void setEthernet(boolean Ethernet) {
        this.Ethernet = Ethernet;
    }

    public boolean isWifi() {
        return Wifi;
    }

    public void setWifi(boolean Wifi) {
        this.Wifi = Wifi;
    }

    public boolean isSoundCard() {
        return SoundCard;
    }

    public void setSoundCard(boolean SoundCard) {
        this.SoundCard = SoundCard;
    }
    
    
    
}