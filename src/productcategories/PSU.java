package productcategories;

public class PSU extends Product {
    private int power; //se Watt
    private String Type;
    private String pfcType; // no idea wtf is this
    private int maxNoiseMade; // monada se dB

    public PSU(int power, String Type, String pfcType, int maxNoiseMade, String price, String name, String code, int guarantee, String availability, String store, String manufacturer, String photo) {
        super(price, name, code, guarantee, availability, store, manufacturer, photo);
        this.power = power;
        this.Type = Type;
        this.pfcType = pfcType;
        this.maxNoiseMade = maxNoiseMade;
    }

    

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public String getPfcType() {
        return pfcType;
    }

    public void setPfcType(String pfcType) {
        this.pfcType = pfcType;
    }

    public int getMaxNoiseMade() {
        return maxNoiseMade;
    }

    public void setMaxNoiseMade(int maxNoiseMade) {
        this.maxNoiseMade = maxNoiseMade;
    }
    
    
    
}
