package productcategories;

public class HardDiskDrive extends Product {
    
    private String capacity;
    private String protocol;
    private String speed;
    private String cacheSize;
    private float formFactor;

    public HardDiskDrive(String capacity, String protocol, String speed, String cacheSize, float formFactor, String price, String name, String code, int guarantee, String availability, String store, String manufacturer, String photo) {
        super(price, name, code, guarantee, availability, store, manufacturer, photo);
        this.capacity = capacity;
        this.protocol = protocol;
        this.speed = speed;
        this.cacheSize = cacheSize;
        this.formFactor = formFactor;
    }

    
    

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getCacheSize() {
        return cacheSize;
    }

    public void setCacheSize(String cacheSize) {
        this.cacheSize = cacheSize;
    }

    public float getFormFactor() {
        return formFactor;
    }

    public void setFormFactor(float formFactor) {
        this.formFactor = formFactor;
    }
    
    
    
}